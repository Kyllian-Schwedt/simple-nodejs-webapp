# Microservices and Monolithic Archi

## Q1: Define what is a Microservices architecture and how does it differ from a Monolithic application.
A Microservices architecture is a design pattern where a single application is composed of multiple indepandent smaller services, each running in its own process and communicating with lightweight mechanisms (resquest or messaging system like redis, RabbitMQ ...). The differences Monolithic application is a single unified unit where all components are interconnected and interdependent.

## Q2: Compare Microservices and Monolithic architectures by listing their respective pros and cons.
### Microservices:
- **Pros:**
  - Scalability: Services can be scaled independently depend of the charge on each modules.
  - Flexibility: Non imposed dependency by a part of the app.
  - Resilience: Failure in one service doesn't affect the entire app.
- **Cons:**
  - Complexity: More complex to deploy and manage.
  - Network latency: Communication between services over the network can cause latency.
  - Data consistency: Ensuring data consistency like user object and authentification across services can be challenging.

### Monolithic:
- **Pros:**
  - Simplicity: develop, test, deploy, scale as a single unit
  - Performance: no latences between modules
- **Cons:**
  - Scalability: The entire application must be scaled even if only one part requires it.
  - Flexibility: Limited to one technology stack.
  - Resilience: A bug in any module can down the entire app.

## Q3: In a Microservices architecture, explain how should the application be split and why.
An application should be split into microservices based on business capabilities, ensuring each service is responsible for a single piece of functionality. This is for independant deployment and scalability...

## Q4: Briefly explain the CAP theorem and its relevance to distributed systems and Microservices.
The CAP theorem states that a distributed system can only simultaneously provide two out of the following three guarantees: Consistency (all nodes see the same data at the same time), Availability (every request receives a response), and Partition tolerance (the system continues to operate despite network partitions).

## Q5: What consequences on the architecture?
The CAP theorem influences architectural decisions in microservices, such as database choice, communication patterns, and service design, to balance between consistency, availability, and partition tolerance.

## Q6: Provide an example of how microservices can enhance scalability in a cloud environment.
Microservices can enhance scalability in a cloud environment by allowing individual services to be scaled independently based on demand. Like Amazon before christmas they need more node beacause lot of people buy or crawl the website.

## Q7: What is statelessness and why is it important in microservices architecture?
Statelessness means that a microservice does not retain any data related to client sessions between requests. It's important because when we have a redundancy of a service, the client can fetch another instance without any problem. 

## Q8: What purposes does an API Gateway serve?
An API Gateway serves as the entry point for clients to a microservices architecture, routing requests to the appropriate services (like GraphQL). The authentication is possible but need to be replicated on each service to avoid security problem.
