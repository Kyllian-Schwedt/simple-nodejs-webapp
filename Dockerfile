FROM node:20

# Set the working directory in the container to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . .

# Make port 8080 expose
EXPOSE 8080

# Define environment variable (as said in the readme)
ENV APPLICATION_INSTANCE=example
# Run the app
CMD ["node", "src/count-server.js"]